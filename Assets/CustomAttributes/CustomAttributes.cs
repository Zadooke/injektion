﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Esse documento possui apenas as classes de Attributes que serão usadas pelo Attribute Analiser
/// 
/// ~ Zadooke, Devora Studios - 2017 ~
/// </summary>


[AttributeUsage(AttributeTargets.Field)] // Diz em qual o tipo de parâmetro do código essa tag poderá ser colocada
public class GetComponent : Attribute
{
    // Da pra colocar funções aqui dentro, para serem usadas na hora de processar esse Attribute

    /// <summary>
    /// Adiciona o Objeto selecionado ao Field do MonoBehaviour
    /// </summary>
    public void SetFieldValue(FieldInfo field, MonoBehaviour monoBehaviour)
    {
        var component = monoBehaviour.GetComponent(field.FieldType);

        // Altera o valor do field com o atributo procurado
        field.SetValue(monoBehaviour, component);
    }
}

/// <summary>
/// Carrega um unico objeto da pasta resources no caminho passado
/// </summary>
[AttributeUsage(AttributeTargets.Field)]
public class ResourcesLoad : Attribute
{
    public readonly string path;

    public ResourcesLoad(string path)
    {
        this.path = path;
    }

    public void Load(FieldInfo field, MonoBehaviour monoBehaviour)
    {
        // Encontra o tipo do Objeto
        var type = field.FieldType;

        // Carrega o componente a ser adicionado
        var component = Resources.Load(path, type);

        // Altera o valor do field com o atributo procurado
        field.SetValue(monoBehaviour, component);
    }
}