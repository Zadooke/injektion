﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// O Attribute Analiser terá que existir em algum lugar da cena, 
/// e funcinará como uma fábrica, injetando as informações de acordo com as instruções do usuário
/// 
/// ~ Zadooke, Devora Studios - 2017 ~
/// </summary>
public class CustomAttributeAnaliser : MonoBehaviour
{
    private void Awake()
    {
        // Pega todos os objetos ativos na cena
        MonoBehaviour[] sceneActive = FindObjectsOfType<MonoBehaviour>();

        foreach (MonoBehaviour item in sceneActive)
        {
            // "Tipo" do script, aka classe
            Type monoType = item.GetType();

            // Pega todos os Fields do script
            FieldInfo[] objectFields = monoType.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance); // Olhar no MSDN os tipos de BindingFlags possíveis aqui, para limitar a busca de Fields na classe

            // Para cada Attribute custom, fazer essa checagem (ainda estou pensando num jeito mais chique de fazer isso)
            for (int i = 0; i < objectFields.Length; i++)
            {
                // [GetComponent]
                GetComponent getcomponent = Attribute.GetCustomAttribute(objectFields[i], typeof(GetComponent)) as GetComponent;
                if (getcomponent != null) // Esse é um campo que possui o atributo procurado
                {
                    // Chamando função só pra mostrar que funciona
                    getcomponent.SetFieldValue(objectFields[i], item);
                }

                // No momento, para qualquer outro tipo de Attribute, refazer o passo acima

                ResourcesLoad resourcesload = Attribute.GetCustomAttribute(objectFields[i], typeof(ResourcesLoad)) as ResourcesLoad;
                if(resourcesload != null)
                {
                    resourcesload.Load(objectFields[i], item);
                }
            }
        }
    }
}
