﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestForCustomAttributes : MonoBehaviour
{
    [Header("Publicos")]
    [GetComponent] public BoxCollider BoxCol;
    [GetComponent] public MeshRenderer MeshRenderer;

    [Header("Privados")]
    [GetComponent, SerializeField]
    private MeshFilter MeshFilter;

    [GetComponent, SerializeField]
    private Rigidbody Rigidbody;

    // Use this for initialization
    void Start()
    {
        // Mostra se está tudo sendo injetado OK

        Debug.Log("Box Collider: " + (BoxCol != null));
        Debug.Log("Box MeshRenderer: " + (MeshRenderer != null));
        Debug.Log("MeshFilter: " + (MeshFilter != null));
        Debug.Log("Rigidbody: " + (Rigidbody != null));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
